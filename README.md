## Observe.ai url-shortner assignment

### Requirements
java11, maven

### Run Tests
```shell script
mvn install

mvn test
```