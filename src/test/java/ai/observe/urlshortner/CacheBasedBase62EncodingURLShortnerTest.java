package ai.observe.urlshortner;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CacheBasedBase62EncodingURLShortnerTest {

    UrlShortner shortner;

    @BeforeEach
    public void setup() {
        shortner = new CacheBasedBase62EncodingURLShortner();
    }

    @Test
    public void testCreation() throws UnidentifiedShortURLException {
        String client1 = "client1";
        String client2 = "client2";
        String longurl = String.valueOf(Math.random());

        String shortenedByClient1 = shortner.shorten(longurl, client1);
        String shortenedByClient1Twice = shortner.shorten(longurl, client1);

        assertEquals(shortenedByClient1, shortenedByClient1Twice);

        String shortenedByClient2 = shortner.shorten(longurl, client2);

        assertNotEquals(shortenedByClient1, shortenedByClient2);

        assertEquals(longurl, shortner.getOriginalURL(shortenedByClient1));
        assertEquals(longurl, shortner.getOriginalURL(shortenedByClient2));


        for (int i = 0; i < 5; i++) {
            shortner.getOriginalURL(shortenedByClient1);
            assertEquals(i+2, shortner.getHitCount(shortenedByClient1));
            assertEquals(1, shortner.getHitCount(shortenedByClient2));
        }

        for (int i = 0; i < 9; i++) {
            shortner.getOriginalURL(shortenedByClient2);
            assertEquals(6, shortner.getHitCount(shortenedByClient1));
            assertEquals(i+2, shortner.getHitCount(shortenedByClient2));
        }

    }
}