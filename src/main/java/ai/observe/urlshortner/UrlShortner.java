package ai.observe.urlshortner;

public interface UrlShortner {

	String shorten(String url, String clientId);

	String getOriginalURL(String shortURL) throws UnidentifiedShortURLException;

	int getHitCount(String shortURL) throws UnidentifiedShortURLException;

}
