package ai.observe.urlshortner;

import io.seruco.encoding.base62.Base62;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class CacheBasedBase62EncodingURLShortner implements  UrlShortner {

    // Counter for generating shortURLs
    AtomicInteger counter;

    // Cache to keep track of hit count of each shortenedURL
    Map<Integer, AtomicInteger> hitCount;

    // Mapping that can be used to look-up original url given a shortened url
    Map<Integer, String> shortToOriginalURL;

    // Mapping to ensure when a client attempts to shorten the same URL, same shortened URL is returned.
    Map<String, Map<String, String>> clientURLs;

    Base62 base62 = Base62.createInstance();

    public CacheBasedBase62EncodingURLShortner() {
        counter = new AtomicInteger();
        hitCount = new ConcurrentHashMap<>();
        shortToOriginalURL = new ConcurrentHashMap<>();
        clientURLs = new ConcurrentHashMap<>();
    }

    @Override
    public String shorten(String url, String clientId) {
        if (clientURLs.containsKey(clientId)) {
            Map<String, String> clientShortenedUrls = clientURLs.get(clientId);

            if(clientShortenedUrls.containsKey(url)) return clientShortenedUrls.get(url);
        } else {
            clientURLs.put(clientId, new ConcurrentHashMap<>());
        }

        int id = counter.addAndGet(1);
        String shortURL = new String(base62.encode(("" + id).getBytes()));

        shortToOriginalURL.put(id, url);
        clientURLs.get(clientId).put(url, shortURL);
        hitCount.put(id, new AtomicInteger(0));
        return shortURL;
    }

    @Override
    public String getOriginalURL(String shortURL) throws UnidentifiedShortURLException {
        int id = getIdForShortURL(shortURL);

        if(!shortToOriginalURL.containsKey(id)) throw new UnidentifiedShortURLException();

        hitCount.get(id).addAndGet(1);
        return shortToOriginalURL.get(id);
    }

    @Override
    public int getHitCount(String shortURL) throws UnidentifiedShortURLException {
        int id = getIdForShortURL(shortURL);
        if(hitCount.containsKey(id)) return hitCount.get(id).intValue();

        throw new UnidentifiedShortURLException();
    }

    private int getIdForShortURL(String shortURL) {
        final byte[] bytes = base62.decode(shortURL.getBytes());
        String decoded = new String(bytes);
        return Integer.parseInt(decoded);
    }
}
